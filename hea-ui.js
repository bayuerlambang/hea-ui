/** 
** build date : 2015-6-11 10:49:17
**/ 
var locTemplate = "bower_components/hea-ui/templates" ; 
var app = angular.module('hea.ui', ['ui.utils.masks']);


app.directive('buttonSave', function () {

    function link() {
        var win = angular.element(window.document);

        win.on('keydown', function (event) {
            void 0;
            if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19) && !(event.which == 83 && event.ctrlKey)) {
                return true;
            } else {
                var btnSave = document.getElementsByName('btnSave')[0];

                btnSave.focus();
                btnSave.click();

                event.preventDefault();
                return false;
            }

        })
    }

    function template(elem, attr) {
        return 'bower_components/hea-ui/templates/button-save.html'
    }

    return {
        restrict: 'E',
        templateUrl: template,
        link: link
    }
});
app.directive('buttonAdd', function () {
    function link() {
        var win = angular.element(window.document);
        win.keydown(function (event) {


            if (!(event.which == 97 && event.ctrlKey) && !(event.which == 1) && !(event.which == 65 && event.ctrlKey)) {
                return true;
            } else {
                var btn = document.getElementsByName('btnEdit')[0];
                btn.focus();
                btn.click();
                event.preventDefault();
                return false;
            }
        })
    }

    function template(elem, attr) {
        return locTemplate + '/button-add.html'
    }

    return {
        restrict: 'E',
        templateUrl: template,
        link: link
    }
});
app.directive('buttonEdit', function () {
    function link() {
        var win = angular.element(window.document);
        win.keydown(function (event) {

            if (!(event.which == 101 && event.ctrlKey) && !(event.which == 69 && event.ctrlKey)) {
                return true;
            } else {
                var btn = document.getElementsByName('btnEdit')[0];
                btn.focus();
                btn.click();
                event.preventDefault();
                return false;
            }
        })
    }

    function template(elem, attr) {
        return locTemplate + '/button-edit.html'
    }

    return {
        restrict: 'E',
        templateUrl: template,
        link: link
    }
});

app.directive('buttonDelete', function () {
    function template(elem, attr) {
        return locTemplate + '/button-delete.html'
    }

    return {
        restrict: 'E',
        templateUrl: template
    }
});

app.directive('buttonCancel', function () {

    function template(elem, attr) {
        return locTemplate + '/button-cancel.html'
    }

    return {
        restrict: 'E',
        templateUrl: template,
        link: function (scope, el, att) {

            var win = angular.element(window.document);
            win.on('keydown', function (event) {
                if (!(event.which == 27 )) {
                    return true;
                } else {
                    var btn = document.getElementsByName('btnCancel')[0];
                    btn.focus();
                    btn.click();
                    event.preventDefault();
                    return false;
                }
            })


        }

    }
});
app.directive('focusIter', function () {

    return function (scope, elem, attrs) {
        //console.log('focusIter','<div name="uiSelect">');
        var atomSelector = attrs.focusIter;


        elem.on('keyup', atomSelector, function (e) {

            var atoms = elem.find(atomSelector), toAtom = null;
            //var uiSelectAtom = window.document.getElementsByClassName('ui-select-toggle') ;
            //atoms.push(uiSelectAtom[0]);


            for (var i = atoms.length - 1; i >= 0; i--) {

                if (atoms[i] === e.target) {
                    if (e.keyCode === 38) {

                        toAtom = atoms[i - 1];

                    } else if (e.keyCode === 40 || e.keyCode == 13) {

                        toAtom = atoms[i + 1];

                    }

                    break;
                }
            }

            if (toAtom) toAtom.focus();

        });

        elem.on('keydown', atomSelector, function (e) {

            if (e.keyCode === 38 || e.keyCode === 40) {
                e.preventDefault();
            }


            if (event.keyCode == 27) {
                var btnCancel = document.getElementsByName('btnCancel')[0];

                if (btnCancel == undefined) {
                    return true;
                } else {
                    btnCancel.focus();
                    btnCancel.click();

                    event.preventDefault();

                }

            }
        });


    };
})

    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {


                if (event.which === 13) {
                    scope.$eval(attrs.ngEnter);
                    scope.$apply(function () {

                    });

                    event.preventDefault();
                }
            });
        };
    })

    .directive('focus', function ($timeout, $parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(attrs.focus, function (newValue, oldValue) {
                    if (newValue) {
                        setTimeout(function () {
                            element[0].focus();
                        }, 100);

                    }
                });
                element.bind("blur", function (e) {
                    $timeout(function () {
                        scope.$apply(attrs.focus + "=false");
                    }, 0);
                });
                element.bind("focus", function (e) {
                    $timeout(function () {
                        scope.$apply(attrs.focus + "=true");
                    }, 0);
                })
            }
        }
    })

    .directive('focusMe', function ($timeout, $parse) {
        return {
            link: function (scope, element, attrs) {
                var model = $parse(attrs.focusMe);
                scope.$watch(model, function (value) {
                    /*console.log('value=',value);*/
                    if (value === true) {
                        $timeout(function () {
                            element[0].focus();
                        });
                    }
                });
                /*element.bind('blur', function() {
                 console.log('blur')
                 scope.$apply(model.assign(scope, false));
                 })*/
            }
        };
    });
app.directive('heaDate', function ($log) {
    return {
        restrict: 'EA',
        require: ['ngModel'],
        replace:true,
        templateUrl: function template(elem, attr) {

            //return 'bower_components/hea-ui/templates/form-group-date.html'
            return  locTemplate + '/form-group-date.html'

        },
        scope: {
            caption: '@',
            inputWidth: '@',
            captionWidth: '@',
            isFocus: '=?',
            isReadonly: '=?'

        },
        controller: function ($scope) {
            $scope.today = function () {
                $scope.dt = new Date();
            };
            $scope.today();

            // Disable weekend selection
            $scope.disabled = function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            };
            $scope.toggleMin = function () {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.toggleMin();
            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events =
                [
                    {
                        date: tomorrow,
                        status: 'full'
                    },
                    {
                        date: afterTomorrow,
                        status: 'partially'
                    }
                ];
            $scope.getDayClass = function (date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            };
        },

        link: function (scope, element, attrs, ctrl) {
            scope.mydate = ctrl.$viewValue;

            // watching ng-model
            scope.dateModel = ctrl.$viewValue;

            scope.$watch('dateModel', function (value) {
                if (ctrl.$viewValue != value) {
                    ctrl[0].$setViewValue(value);
                }
            });
        }
    }
});
app.directive('txtKode',function($filter){
    function pad(value, length) {
        return (value.toString().length < length) ? pad("0"+value, length):value;
    }
    function link(scope, element, attributes) {

        /*wathing input here*/
        scope.$watch(attributes.ngModel, function (value, oldValue) {

            value = String(value);

            // replace value, number only
            var number = value.replace(/[^0-9]+/g, '');
            //  format value dengan filter rekeningformat
            var inputValue = $filter('rekeningformat')(number, 'kode');

            // inject element with new value
            element.val(inputValue)

        });

        element.on('blur', function () {
            // cek format
            var elValue = element.val();
            if(elValue !== ''){
                var vaValue = elValue.split('.');
                if(vaValue[1] == '' || vaValue[1] == undefined){
                    //alert('No Rekening Belum Lenkap') ;
                    element.focus();
                }else{
                    var newValue = vaValue[0] +"."+  pad(vaValue[1],6);

                    element.val(newValue);
                }
            }

        })
    }

    return {
        restrict: 'A',
        require: '?ngModel',
        link: link
    }
});

app.directive('txtRekening', function ($filter) {
    function pad(value, length) {
        return (value.toString().length < length) ? pad("0"+value, length):value;
    }
    function link(scope, element, attributes) {

        /*wathing input here*/
        scope.$watch(attributes.ngModel, function (value, oldValue) {

            value = String(value);

            // replace value, number only
            var number = value.replace(/[^0-9]+/g, '');

            //  format value dengan filter rekeningformat

            var inputValue = $filter('rekeningformat')(number, 'rekening');



            // inject element with new value
            element.val(inputValue);

        });

        element.on('blur', function () {
            // cek format
            var elValue = element.val();
            if(elValue !== ''){
                var vaValue = elValue.split('.');
                if(vaValue[2] == '' || vaValue[2] == undefined){
                    //alert('No Rekening Belum Lenkap') ;
                    element.focus();
                }else{
                    var newValue = vaValue[0] +"."+ vaValue[1] + "." + pad(vaValue[2],6);

                    element.val(newValue);
                }
            }

        })
    }

    return {
        restrict: 'A',
        require: '?ngModel',

        link: link

    }
});

app.filter('rekeningformat', function () {



    return function (number, type) {
        if (!number) {
            return '';
        }

        if (!type || type == '') {
            return '';
        }


        if (type == 'rekening') {
            var formattedNumber = number;

            // format ##.##.######


            var KodeCabang = number.substr(0, 2);
            var KodeProduk = number.substr(2, 2);
            var NoUrut = number.substr(4,6);
            //var Frew = number.substring(10, 12);


            if (KodeCabang) {
                formattedNumber = KodeCabang;
                if (KodeCabang.length == 2) {
                    formattedNumber += '.';
                }
            }
            if (KodeProduk) {
                formattedNumber += KodeProduk;
                if (KodeProduk.length == 2) {
                    formattedNumber += '.';
                }
            }

            if (NoUrut) {
                formattedNumber += NoUrut;

                if(NoUrut.length > 6){
                    return ;
                }


            }




        } else {
            // format kode
            var formattedNumber = number;

            // format ##.######


            var KodeCabang = number.substring(0, 2);
            var NoUrut = number.substring(2, 8);


            if (KodeCabang) {
                formattedNumber = KodeCabang;
                if (KodeCabang.length == 2) {
                    formattedNumber += '.';
                }
            }

            if (NoUrut) {
                formattedNumber += NoUrut;
                if (NoUrut.length > 6) {
                    formattedNumber += '';
                }

            }


        }


        return formattedNumber;

    }
});
app.directive('txtSelect', function ($timeout) {
    return{
        restrict: 'E',
        require: ['ngModel'],
        replace:true,
        templateUrl: function template(elem, attr) {
            //return 'bower_components/hea-ui/templates/form-group-select.html'
            return locTemplate + '/form-group-select.html'
        },
        scope: {
            caption: '@',
            selectWidth: '@',
            captionWidth: '@',
            items : '=',
            itemsSelect : '@',
            itemsLabel : '@',
            itemsLabel2 : '@',
            isFocus: '=?',
            isReadonly: '=?',
            itemTest: '@'
        },

        controller: function ($scope,$element, $transclude) {
            $scope.showGrid = false ;
            $scope.datas = $scope.items ;


        },
        link: function (scope,element,attrs,ngModel) {

            scope.getDropdown = function(){
                void 0 ;
            }


            scope.$watch('items', function (value) {



                scope.items = value ;
                angular.forEach(scope.items, function (v,k) {
                    if(scope.itemsLabel !== undefined){
                        scope.items[k].display = v[scope.itemsSelect] + " - " + v[scope.itemsLabel] ;
                    }else{
                        scope.items[k].display = v[scope.itemsSelect] ;
                    }

                })

            });

            // watching ng-model
            scope.selectModel = ngModel[0].$viewValue;

            scope.$watch('selectModel', function (value) {
                if (ngModel[0].$viewValue != value) {
                    ngModel[0].$setViewValue(value[scope.itemsSelect]);
                }


            });


            scope.lReadOnly = scope.isReadonly;


            // watching isReadonly change
            scope.$watch('isReadonly', function (value) {
                scope.lReadOnly = value;
            });


            scope.lFocus = scope.isFocus;




            scope.$watch('isFocus', function (newValue, oldValue) {

                scope.lFocus = newValue;

            });

        }
    }
});

app.directive('txt', function ($timeout) {

    return {
        restrict: 'EA',
        require: ['ngModel'],
        scope: {
            caption: '@',
            inputWidth: '@',
            captionWidth: '@',
            digit: '@',
            isFocus: '=?',
            isReadonly: '=?',
            type: '@',
            digit: '@',
            holder: '@'


        },
        replace: true,
        templateUrl: function template(elem, attr) {

            //return 'bower_components/hea-ui/templates/form-group.html'
            return  locTemplate + '/form-group.html'
        },
        controller: function ($scope) {


        },
        link: function (scope, el, attr, ctrl) {
            scope.holder = 0 ;
            if(scope.digit !== undefined){
                if(scope.type == 'desimal'){
                    var sparator = ',';
                }else{
                    var sparator = '.';
                }
                var placeholder = "0" ;
                switch(scope.digit){
                    case "1" :
                        scope.holder =  placeholder + sparator + "0";
                        break;
                    case "2":
                        scope.holder = placeholder + sparator + "00";
                        break;
                    case "3" :
                        scope.holder = placeholder + sparator + "000";
                        break;
                }

                void 0;

            }




            if (scope.type == undefined) {
                scope.type = 'text';
            }

            scope.txtModel = ctrl.$viewValue;

            // watching ng-model
            scope.$watch('txtModel', function (value) {

                if (ctrl.$viewValue != value) {

                    ctrl[0].$setViewValue(value);

                }
            });


            scope.lReadOnly = scope.isReadonly;


            // watching isReadonly change
            scope.$watch('isReadonly', function (value) {
                scope.lReadOnly = value;
            });


            scope.lFocus = scope.isFocus;


            scope.$watch('isFocus', function (newValue, oldValue) {

                scope.lFocus = newValue;

            });

            scope.enterKey = ctrl.$viewValue;

            scope.$watch('enterKey', function(value){
                if (ctrl[0].$viewValue != value) {

                    ctrl[0].$setViewValue(value);

                }
            })


        }
    }
})/**
 * Created by Bayu Erlambang on 6/2/2015.
 */

app.directive('typeaheadFocus', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ngModel) {

            //trigger the popup on 'click' because 'focus'
            //is also triggered after the item selection
            element.bind('click', function () {

                var viewValue = ngModel.$viewValue;

                //restore to null value so that the typeahead can detect a change
                if (ngModel.$viewValue == ' ') {
                    ngModel.$setViewValue(null);
                }

                //force trigger the popup
                ngModel.$setViewValue(' ');

                //set the actual value in case there was already a value in the input
                ngModel.$setViewValue(viewValue || ' ');
            });

            //compare function that treats the empty space as a match
            scope.emptyOrMatch = function (actual, expected) {
                if (expected == ' ') {
                    return true;
                }
                return actual.indexOf(expected) > -1;
            };
        }
    };
});